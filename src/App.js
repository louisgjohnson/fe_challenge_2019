import React, { Component } from 'react';
import Table from './components/Table';
import data from './assets/data.json';
import search from './utils/search';
import sort from './utils/sort';
import styled from 'styled-components';
import SearchInput from './components/SearchInput';
import AddUserModal from './components/AddUser';
import StyledButton from './components/Button';

import 'normalize.css';

const AppContainer = styled.div`
  width: 100%;
  height: 100%;
  padding: 1rem;
  background: linear-gradient(to bottom, white 50%, #a4e8ff 50%);
`

const Wrapper = styled.div`
  background: white;
  box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
  border-radius: 8px;
  padding: 2rem 0 1rem;
  margin: 1rem 0;
  min-height: 400px;
`

const Heading = styled.h1`
  margin: 0;
  text-transform: uppercase;
  font-weight: bold;
`

const HeaderContainer = styled.div`
  display: flex;
  padding-top: 100px;
`;

const ButtonContainer = styled.div`
  margin: auto 0 auto auto;
`


class App extends Component {

  constructor() {
    super();
    this.state = {
      employees: data.employees,
      // maintained list to add users to and edit users on
      baseEmployees: data.employees,
      sortBy: 'asc',
      activeSortCol: null,
      showModal: false
    }
  }

  sortColumn = (index) => {
    const sortedEmployees = this.state.employees.sort((a, b) => {
      const keyToSort = Object.keys(a)[index];
      const valueA = a[keyToSort];
      const valueB = b[keyToSort];
      return sort(valueA, valueB);
    });
    if (index === this.state.activeSortCol && this.state.sortBy === 'asc') {
      this.setState({
        employees: sortedEmployees.reverse(),
        sortBy: 'desc'
      })
    } else {
      this.setState({
        employees: sortedEmployees,
        activeSortCol: index,
        sortBy: 'asc'
      })
    }
    
  }

  onButtonPress = () => {
    this.setState({
      showModal: true
    })
  }

  onModalClose = () => {
    this.setState({
      showModal: false
    })
  }

  searchData = (phrase) => {
    let employees = this.state.baseEmployees;
    if (phrase) {
      employees = search(employees, phrase);
    }
    this.setState({
      employees
    })
  }

  addNewEmployee = (newEmployee) => {
    const { baseEmployees } = this.state;
    const id = baseEmployees.length + 1;
    const employeeToAdd = {...newEmployee, id };
    const newBase = [...baseEmployees, employeeToAdd];
    this.setState({
      baseEmployees: newBase,
      employees: newBase
    });
    // handle current sort and search because it will get reset here
  }

  render() {
    const { activeSortCol, showModal, employees } = this.state;
    return (
      <AppContainer>
        <HeaderContainer>
          <div>
            <Heading>Biarri</Heading>
            <Heading>Groceries</Heading>
          </div>
          <ButtonContainer>
            <StyledButton onClick={this.onButtonPress}>Add Employee</StyledButton>
          </ButtonContainer>
        </HeaderContainer>
        <Wrapper>
          <SearchInput onChange={this.searchData} />
          <Table active={activeSortCol} data={employees} employmentStatusMap={data.employment_statuses} sort={this.sortColumn} />
        </Wrapper>
        <AddUserModal show={showModal} close={this.onModalClose} addNewEmployee={this.addNewEmployee} />
      </AppContainer>
    )
  }
}

export default App;

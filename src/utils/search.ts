type employeesData = {
    firstName: string,
    lastName: string
    id: number
}[]

const search = (employees: employeesData, phase: string) => (
    employees.filter(employee => {
        let match = false;
        for (let [_key, value] of Object.entries(employee)) {
            if (value.toString().toLowerCase().includes(phase)) {
                match = true;
                break;
            }
        }
        return match;
    })
)

export default search;
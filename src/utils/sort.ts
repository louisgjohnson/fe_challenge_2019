type SortType = string | number;

const sort = (a: SortType, b: SortType) => {
    let valueA: SortType | Date = a;
    let valueB: SortType | Date = b;
    if (typeof a === 'string' && typeof b === 'string') {
        if (isDate(a) && isDate(b)) {
            valueA = new Date(a);
            valueB = new Date(b);
        } else {
            valueA = a.toLowerCase();
            valueB = b.toLowerCase();
        }
    }
    if (valueA != valueB) {
        return valueA > valueB ? 1 : -1;
    }
    return 0; 
};


// probably should be a better check but will be suffice for the test considering the data's format
const isDate = (possibleDate: string) => (
    possibleDate.length === 10 && possibleDate[2] === '/' && possibleDate[5] === '/'
);

export default sort;
import React from 'react';
import styled, { css } from 'styled-components'

export const StyledRow = styled.tr`
    padding: 0 1rem 2rem;
    display: flex;
    width: 100%;
    ${props => props.bold && css`
        font-weight: bold;
    `}
`


const Row = () => {
    return null;
}


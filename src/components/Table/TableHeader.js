import React from 'react';
import { StyledRow } from './Row';
import { StyledCell } from './Cell';

const TableHead = ({ headings, sort }) => {


    const onClick = (index) => {
        console.log('fires');
        return () => sort(index)
    }

    return (
        <thead>
            <StyledRow bold>
                {headings.map((heading, index) => <StyledCell onClick={onClick(index)} key={heading}>{heading}</StyledCell>)}
            </StyledRow>
        </thead>
    )
}



export default TableHead;
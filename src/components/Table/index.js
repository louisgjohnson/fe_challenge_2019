import React from 'react';
import PropTypes from 'prop-types';
import TableHeader from './TableHeader';
import { StyledRow } from './Row';
import { StyledCell } from './Cell';
import styled, { css } from 'styled-components'


const headings = ['ID', 'First Name', 'Last Name', 'Date Of Birth', 'Date Hired', 'Employment Status'];

const StyledTable = styled.table`
    border-top: 1px solid #f3f3f3;
    width: 100%;
    display: flex;
    flex-direction: column;
    padding: 1rem;
    margin-top: 0.5rem;
`

const StyledTBody = styled.tbody`
    width: 100%;
    display: flex;
    flex-direction: column;
`

const EmployStatus = styled.div`
    border-radius: 6px;
    color: white;
    font-weight: bold;
    display: inline-flex;
    padding: 2px 5px;
    ${props => `background: ${props.colour}`}
`


const Table = ({ data, employmentStatusMap, sort, active }) => {
    const FormatEmployeeStatus = (currentStatusId) => {
        const status = employmentStatusMap.find(employmentStatus => employmentStatus.id === currentStatusId);
        return <EmployStatus colour={status.colour}>{status.name}</EmployStatus>
    }
    return (
        <StyledTable>
            <TableHeader headings={headings} sort={sort} active={active} />
            <StyledTBody>
                {data.map(({ id, first_name, last_name, date_of_birth, date_hired, employment_status }) => (
                    <StyledRow key={id}>
                        <StyledCell>{id}</StyledCell>
                        <StyledCell>{first_name}</StyledCell>
                        <StyledCell>{last_name}</StyledCell>
                        <StyledCell>{date_of_birth}</StyledCell>
                        <StyledCell>{date_hired}</StyledCell>
                        <StyledCell>{FormatEmployeeStatus(employment_status)}</StyledCell>
                    </StyledRow>
                ))}
            </StyledTBody>
        </StyledTable>
    )
}

Table.propTypes = {
    employmentStatusMap: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number,
            name: PropTypes.string,
            colour: PropTypes.string
        })
    ),
    data: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number,
            first_name: PropTypes.string,
            last_name: PropTypes.string,
            date_of_birth: PropTypes.string,
            date_hired: PropTypes.string,
            employment_status: PropTypes.number
        })
    )
}

export default Table;
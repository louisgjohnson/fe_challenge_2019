import styled from 'styled-components';


const StyledButton = styled.button `
    background: #78D363
    border: 0;
    color: white;
    border-radius: 4px;
    padding: 10px 20px;
    font-weight: bold;
    box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
    cursor: pointer;
`;

export default StyledButton;
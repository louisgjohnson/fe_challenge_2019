import React from 'react';
import styled from 'styled-components';
import StyledButton from '../Button'

const Modal = styled.div`
    position: absolute;
    min-width: 500px;
    min-height: 500px;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translateX(-50%) translateY(-50%);
    background: white;
    box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
    border-radius: 8px;
    padding: 1rem;
`

const Header = styled.h1`
    text-align: center;
`

const Form = styled.div`
    display: flex;
    flex-direction: column;
`;

const RadioButtonItem = styled.div`
    flex: 1;
    input {
        margin-right: 0.5rem;
    }
`

const ButtonContainer = styled.div`
    margin: auto;
    padding: 1rem;
`

const RadioButtonContainer = styled.div`
    display: flex;
    
`


class AddUserModal extends React.Component {
    constructor() {
        super();
        this.state = {
            firstName: 'Test',
            lastName: 'Test',
            dateOfBirth: '01/01/1990',
            dateHired: '01/01/2019',
            status: 'active'
        }
    }

    mapStatusToDataValue = (status) => {
        return {
            active: 1,
            inactive: 2,
            pending: 3
        }[status];
    }

    onInputChanged(key) {
        return (event) => {
            this.setState({
                [key]: event.target.value
            })
        }
    }

    onClose = () => {
        const { firstName, lastName, dateOfBirth, dateHired, status } = this.state;
        this.props.addNewEmployee({
            first_name: firstName,
            last_name: lastName,
            date_of_birth: dateOfBirth,
            date_hired: dateHired,
            employment_status: this.mapStatusToDataValue(status)
        })
        this.props.close();
    }


    render() {
        if (!this.props.show) return null;
        const { firstName, lastName, dateOfBirth, dateHired, status } = this.state;
        return (
            <Modal>
                <Header>Add Employee</Header>
                <Form>
                    <p>First Name</p>
                    <input value={firstName} onChange={this.onInputChanged('firstName')} />
                    <p>Last Name</p>
                    <input value={lastName} onChange={this.onInputChanged('lastName')} />
                    <p>Date Of Birth</p>
                    <input value={dateOfBirth} onChange={this.onInputChanged('dateOfBirth')} />
                    <p>Date Hired</p>
                    <input value={dateHired} onChange={this.onInputChanged('dateHired')} />
                    <p>Employment Status</p>
                    <RadioButtonContainer>
                        <RadioButtonItem>
                            <input
                                type="radio"
                                id="active"
                                name="status"
                                value="active"
                                checked={status === 'active'}
                                onChange={this.onInputChanged('status')}
                            />
                            <label htmlFor="active">Active</label>
                        </RadioButtonItem>
                        <RadioButtonItem>
                            <input type="radio" id="inactive" name="status" value="inactive" checked={status === 'inactive'} onChange={this.onInputChanged('status')} />
                            <label htmlFor="inactive">Inactive</label>
                        </RadioButtonItem>
                        <RadioButtonItem>
                            <input type="radio" id="pending" name="status" value="pending" checked={status === 'pending'} onChange={this.onInputChanged('status')} />
                            <label htmlFor="pending">pending</label>
                        </RadioButtonItem>
                    </RadioButtonContainer>
                    <ButtonContainer>
                        <StyledButton onClick={this.onClose}>Add new Employee</StyledButton>
                    </ButtonContainer>
                    
                </Form>
            </Modal>
        )
    }
}

export default AddUserModal;
import React from 'react';
import styled from 'styled-components'
import icon from '../../assets/search.svg';

const StyledInput = styled.input`
    width: 100%;
    border: 0;
    height: 2rem;
    font-size: 20px;
    :focus {
        outline: none;
    }
`

const Icon = styled.img`
    width: 25px;
    height: 25px;
    margin: auto 0.5rem;
`

const Container = styled.div`
    display: flex;
`


const SearchInput = ({ onChange }) => {
    const onChangeInput = (event) => {
        onChange(event.target.value);
    }
    
    return (
        <Container>
            <Icon src={icon} />
            <StyledInput onChange={onChangeInput} placeholder="Search for.." />
        </Container>
        
    )
}

export default SearchInput;
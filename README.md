# Biarri front end challenge

### technology
React (installed with react-create-app)
Typescript for the util files (I was going to use Typescript for the react side but ending up ditching it because the extra time it would of taken.)

## running the app
`yarn` or `npm install`

`yarn start` or `npm start`

The app should run on `http://localhost:3000/`

## issues

- Can't Edit rows
- Overall styling on the UI (its currently okay but a bit rushed)
- Adding a new user removes the search/sorting atm
- Adding some styling to show sorting order e.g an arrow icon or something like that
- Code is a bit rushed overall
- Cant close modal
- Sorting with dates isn't correct
- For sorting & searching table by string, I should be handling that by method that returns and renders the data instead of maintaining two arrays of employee data:

The value of the search input should be stored in the state then use the search method to filter out the array values after the sorting has been done

```
	getDisplayData = () => {
		return search(this.state.employees.sort(/* sort logic */), /* search input value */);
	}

```















